import React, { useState, useEffect, Fragment } from 'react';
import classes from './RandomUsers.module.css';

export default function RandomUser() {
    const [user, setUser] = useState([]);
    const [error, setError] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const userClickedHandler = (event) => {
        event.preventDefault();
        console.log(`${event.target.key} clicked`);
      }

    async function fetchRandomUser() {
        try {
            await fetch('https://randomuser.me/api/?results=5')
            .then(results => {
                return results.json();
            })
            
            .then(data => {
                let user = data.results.map((user) => {
                    const userData = <div className={classes.beside} key={user.name.first + user.name.last} onClick={userClickedHandler}>
                            <img  className={classes.picture} src={user.picture.large} alt="" />
                            <div>
                                <h2>{user.name.first} {user.name.last}</h2>
                                <p>Email: {user.email}</p>
                                <p>Phone: {user.phone}</p>
                                <p>Address: {user.location.street.number} {user.location.street.name}, {user.location.city} {user.location.state} {user.location.postcode}</p>
                                <p>Birthday: {user.dob.date}</p>
                            </div>
                            </div>;
                    return userData;
                });
                setUser(user);
                setIsLoading(false);
            });
        }
        catch(error) {
            setError(error);
            setIsLoading(false);
        }
    }

    useEffect(() => {
        fetchRandomUser();
    }, []);
    return(
        <Fragment>
            {error && <p>{error.message}</p>}
            {isLoading && <p>Loading...</p>}
            {user}
        </Fragment>
    );
} 
