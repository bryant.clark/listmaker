import { Fragment, useRef, useState } from 'react';


import Card from '../ui/Card';
import RandomUser from '../users/RandomUser';
import classes from './NameInput.module.css';

const NameInput = (props) => {
  const [entered, setEntered] = useState(false);

  const nameInputRef = useRef();

  const submitHandler = (event) => {
    event.preventDefault();
    setEntered(true)
  }

  const backHandler = (event) => {
    event.preventDefault();
    setEntered(false)
  }

  return (
    <Fragment>
      {!entered&&<header className={classes.header}>
        <h1>Home</h1>
        </header>}
      {!entered&&<Card>
      <form className={classes.form} onSubmit={submitHandler}>
          <div className={classes.control}>
            <label htmlFor='name'>Your Name</label>
            <input type='text' required id='name' ref={nameInputRef} />
          </div>
          <div className={classes.actions}>
            <button>Enter</button>
          </div> 
        </form>
      </Card>}
      {entered&&<header className={classes.header}>
        <h1>{nameInputRef.current.value}</h1>
        </header>}
      {entered&&<RandomUser />}
      {entered&&<div className={classes.back}>
      <button onClick={backHandler}>Back</button>
      </div>}
    </Fragment>
  );
}

export default NameInput;
