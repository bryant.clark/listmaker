import './App.css';
import NameInput from './components/input/NameInput';

function App() {
  return (
    <NameInput />
  );
}

export default App;
